# raspberry

raspberry pi

![](media/rpi0.png)


![](media/rpi3b+.png)


![](media/rpi4b.png)

![](media/2018-raspberry-pi-4-b-2-gb-4-x-1-5-ghz-raspberry-pi.jpg)



# Headless WPA

Copy on partition 1 (fat)

````
country=us
update_config=1
ctrl_interface=/var/run/wpa_supplicant

network={
 scan_ssid=1
 ssid="THEWIFIROUTER"
 psk="THESUPERWPAKEY"
}
````


# NetBSD for PI?

https://gitlab.com/openbsd98324/armv7
